# Emojis by luka

## ms_robot_angry.png
![](luka/ms_robot_angry.png)

## ms_robot_cowbot.png
![](luka/ms_robot_cowbot.png)

## ms_robot_display_shades.png
![](luka/ms_robot_display_shades.png)

## ms_robot_error.png
![](luka/ms_robot_error.png)

## ms_robot_fingerguns.png
![](luka/ms_robot_fingerguns.png)

## ms_robot_grin.png
![](luka/ms_robot_grin.png)

## ms_robot_headpats.png
![](luka/ms_robot_headpats.png)

## ms_robot_kiss.png
![](luka/ms_robot_kiss.png)

## ms_robot_pats.png
![](luka/ms_robot_pats.png)

## ms_robot_sleep_mode.png
![](luka/ms_robot_sleep_mode.png)

## ms_robot_small.png
![](luka/ms_robot_small.png)

## ms_robot_surprised.png
![](luka/ms_robot_surprised.png)

## ms_robot_thinkgrin.png
![](luka/ms_robot_thinkgrin.png)

## ms_robot_thinking.png
![](luka/ms_robot_thinking.png)

## ms_robot_upside_down.png
![](luka/ms_robot_upside_down.png)

