AUTHORS = 666666t gardenbot lavender luka lun-4 meri 

.PHONY = package

package:
	for folder in $(AUTHORS); do \
	    echo $$folder; \
	    cd $$folder; \
	    zip -9r ../robomojis.zip ./*; \
	    cd ..; \
	done

package-tiny:
	for folder in $(AUTHORS); do \
	    echo $$folder; \
	    mkdir -p ./$$folder-tiny; \
	    tiny_files=$$(find $$folder -type f ! -size +50k); \
    	    for tiny_file in $$tiny_files; do \
    	        tiny_filename=$$(basename $$tiny_file); \
                cp $$tiny_file $$folder-tiny/$$tiny_filename; \
            done; \
	    big_files=$$(find $$folder -type f -size +50k); \
    	    for big_file in $$big_files; do \
    	        gm $$big_file convert -format png -quality 50 $$folder-tiny/$$big_file; \
            done; \
	    cd $$folder-tiny; \
	    zip -9r ../robomojis.zip ./*; \
	    cd ..; \
	done

clean:
	rm -f ./robomojis.zip

preview:
	preview=preview.md; \
	echo -n > $$preview; \
	for folder in $(AUTHORS); do \
		author_readme=$$folder/README.md; \
		echo -n > $$author_readme; \
		echo "# Emojis by $$folder\n" | tee -a $$preview | tee -a $$author_readme; \
		for emoji_path in $$folder/*.png; do \
			emoji_filename=$$(basename $$emoji_path); \
			echo "## $$emoji_filename" | tee -a $$preview | tee -a $$author_readme; \
			echo "![]($$emoji_path)\n" | tee -a $$preview | tee -a $$author_readme; \
		done; \
	done
