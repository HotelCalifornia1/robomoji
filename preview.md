# Emojis by gardenbot

## ms_robot_bee.png
![](gardenbot/ms_robot_bee.png)

## ms_robot_seedling.png
![](gardenbot/ms_robot_seedling.png)

## ms_robot_sunflower.png
![](gardenbot/ms_robot_sunflower.png)

## ms_robot_thinkthonk.png
![](gardenbot/ms_robot_thinkthonk.png)

## ms_robot_upside_down_display.png
![](gardenbot/ms_robot_upside_down_display.png)

# Emojis by lavender

## ms_robot_how.png
![](lavender/ms_robot_how.png)

## ms_robot_pensive.png
![](lavender/ms_robot_pensive.png)

## ms_robot_sad.png
![](lavender/ms_robot_sad.png)

# Emojis by luka

## ms_robot_angry.png
![](luka/ms_robot_angry.png)

## ms_robot_cowbot.png
![](luka/ms_robot_cowbot.png)

## ms_robot_display_shades.png
![](luka/ms_robot_display_shades.png)

## ms_robot_error.png
![](luka/ms_robot_error.png)

## ms_robot_fingerguns.png
![](luka/ms_robot_fingerguns.png)

## ms_robot_grin.png
![](luka/ms_robot_grin.png)

## ms_robot_headpats.png
![](luka/ms_robot_headpats.png)

## ms_robot_kiss.png
![](luka/ms_robot_kiss.png)

## ms_robot_pats.png
![](luka/ms_robot_pats.png)

## ms_robot_sleep_mode.png
![](luka/ms_robot_sleep_mode.png)

## ms_robot_small.png
![](luka/ms_robot_small.png)

## ms_robot_surprised.png
![](luka/ms_robot_surprised.png)

## ms_robot_thinkgrin.png
![](luka/ms_robot_thinkgrin.png)

## ms_robot_thinking.png
![](luka/ms_robot_thinking.png)

## ms_robot_upside_down.png
![](luka/ms_robot_upside_down.png)

# Emojis by lun-4

## ms_robot.png
![](lun-4/ms_robot.png)

## ms_robot_blobthink.png
![](lun-4/ms_robot_blobthink.png)

## ms_robot_blushy_4.png
![](lun-4/ms_robot_blushy_4.png)

## ms_robot_blushy_crushy_4.png
![](lun-4/ms_robot_blushy_crushy_4.png)

## ms_robot_cat.png
![](lun-4/ms_robot_cat.png)

## ms_robot_display_heart.png
![](lun-4/ms_robot_display_heart.png)

## ms_robot_dress.png
![](lun-4/ms_robot_dress.png)

## ms_robot_dress_hearts.png
![](lun-4/ms_robot_dress_hearts.png)

## ms_robot_dress_white.png
![](lun-4/ms_robot_dress_white.png)

## ms_robot_fixing_coffee.png
![](lun-4/ms_robot_fixing_coffee.png)

## ms_robot_heart_cybre_lesbian.png
![](lun-4/ms_robot_heart_cybre_lesbian.png)

## ms_robot_heart_eyes.png
![](lun-4/ms_robot_heart_eyes.png)

## ms_robot_pats.png
![](lun-4/ms_robot_pats.png)

## ms_robot_peek.png
![](lun-4/ms_robot_peek.png)

## ms_robot_ribbon.png
![](lun-4/ms_robot_ribbon.png)

## ms_robot_says.png
![](lun-4/ms_robot_says.png)

## ms_robot_says_2.png
![](lun-4/ms_robot_says_2.png)

## ms_robot_sleep.png
![](lun-4/ms_robot_sleep.png)

## ms_robot_think.png
![](lun-4/ms_robot_think.png)

# Emojis by meri

## ms_robot_blissful.png
![](meri/ms_robot_blissful.png)

## ms_robot_cheery.png
![](meri/ms_robot_cheery.png)

## ms_robot_display_kiss.png
![](meri/ms_robot_display_kiss.png)

## ms_robot_hearts.png
![](meri/ms_robot_hearts.png)

## ms_robot_wave.png
![](meri/ms_robot_wave.png)

# Emojis by 666666t
## ms_robot_waaaugh.png
![](666666t/ms_robot_waaaugh.png)

# Emojis by hotel
## ms_robot_plead.png
![](hotel/ms_robot_plead.png)
